package com.cybratech;

import com.cybratech.creator.*;
import com.cybratech.dto.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class Main {

    /**
     * Entrypoint.
     * @param args
     */
    public static void main(String[] args) throws IOException {
        //Kategorien
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("inserts.sql")));
        KategorieCreator kategorieCreator = new KategorieCreator();
        List<Kategorie> kategorieList = kategorieCreator.createCategories(40);
        List<String> categoryInserts = kategorieCreator.createCategoryInserts(kategorieList);
        for(String s: categoryInserts){
            out.println(s);
            System.out.println(s);
        }
        //Adressen
        AdresseCreator adresseCreator = new AdresseCreator();
        List<Adresse> adresseList = adresseCreator.createAdressen(100);
        List<String> adresseInserts = adresseCreator.createAdressenInserts(adresseList);
        for(String s: adresseInserts){
            out.println(s);
            System.out.println(s);
        }
        //Nutzer
        BenutzerCreator benutzerCreator = new BenutzerCreator();
        List<Benutzer> benutzerList = benutzerCreator.createBenutzer(100);
        List<String> benuterInserts = benutzerCreator.createBenutzerInserts(benutzerList);
        for(String s: benuterInserts){
            out.println(s);
            System.out.println(s);
        }
        //Kaeufer
        KaeuferCreator kaeuferCreator = new KaeuferCreator();
        List<Kaeufer> kaeuferList = kaeuferCreator.createKaeufer(benutzerList.subList(0,69));
        List<String> kaeuferInserts = kaeuferCreator.createKaeuferInserts(kaeuferList);
        for(String s: kaeuferInserts){
            out.println(s);
            System.out.println(s);
        }
        //Verkaeufer
        VerkaeuferCreator verkaeuferCreator = new VerkaeuferCreator();
        List<Verkaeufer> verkaeuferList =verkaeuferCreator.createVerkaeufer(benutzerList.subList(40,99));
        List<String> verkaeuferInserts = verkaeuferCreator.createVerkaeuferInserts(verkaeuferList);
        for(String s: verkaeuferInserts){
            out.println(s);
            System.out.println(s);
        }
        //Produkt
        ProduktCreator produktCreator = new ProduktCreator();
        List<Produkt> produktList = produktCreator.createProdukte(kategorieList,200);
        List<String> produktInserts = produktCreator.createProdukteInsert(produktList);
        for(String s: produktInserts){
            out.println(s);
            System.out.println(s);
        }
        //Auktion
        AuktionCreator auktionCreator = new AuktionCreator();
        List<Auktion> auktionList = auktionCreator.createAuktionen(1000, produktList, verkaeuferList);
        List<String> auktionInserts = auktionCreator.createAuktionenInserts(auktionList);
        for(String s: auktionInserts){
            out.println(s);
            System.out.println(s);
        }
        //Ansichten und Gebote
        AnsichtGeboteCreator ansichtGeboteCreator = new AnsichtGeboteCreator();
        List<Auktion> auktionenMitAnsichtenUndGeboten = ansichtGeboteCreator.createAnsichtenAndGebote(auktionList, kaeuferList);
        List<String> ansichtenUndGeboteInserts = ansichtGeboteCreator.createAnsichtsAndGeboteInserts(auktionenMitAnsichtenUndGeboten);
        for(String s: ansichtenUndGeboteInserts){
            out.println(s);
            System.out.println(s);
        }
        //Versand und Bewertungen
        VersandBewertungCreator versandBewertungCreator = new VersandBewertungCreator();
        List<Auktion> auktionenMitBewertungenUndVersand = versandBewertungCreator.createVersandAndBewertung(auktionenMitAnsichtenUndGeboten);
        List<String> auktionenMitBewertungenUndVersandInserts = versandBewertungCreator.createVersandAndBewertungInserts(auktionenMitBewertungenUndVersand);
        for(String s: auktionenMitBewertungenUndVersandInserts){
            out.println(s);
            System.out.println(s);
        }
    }
}
