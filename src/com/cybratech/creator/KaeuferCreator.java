package com.cybratech.creator;

import com.cybratech.InsertStatementCreationUtil;
import com.cybratech.dto.Benutzer;
import com.cybratech.dto.Kaeufer;

import java.util.LinkedList;
import java.util.List;

public class KaeuferCreator {
    private static final String TABLENAME ="kaeufer";

    public List<Kaeufer> createKaeufer(List<Benutzer> benutzers){
        List<Kaeufer> kaeufers = new LinkedList<>();
        for(Benutzer nutzer: benutzers){
            Kaeufer kaeufer = new Kaeufer(nutzer.getId());
            kaeufers.add(kaeufer);
        }
        return kaeufers;
    }

    public List<String> createKaeuferInserts(List<Kaeufer> kaeufers){
        String[] columns = new String[]{"id"};
        List<String[]> attributes = new LinkedList<>();
        for (Kaeufer kaeufer : kaeufers) {
            attributes.add(new String[]{String.valueOf(kaeufer.getId())});
        }
        return InsertStatementCreationUtil.createInsert(columns, attributes, TABLENAME);
    }
}
