package com.cybratech.creator;

import com.cybratech.DateCreationUtil;
import com.cybratech.InsertStatementCreationUtil;
import com.cybratech.StringWrapUtil;
import com.cybratech.dto.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class VersandBewertungCreator {

    public static final String TABLENAME_VERSAND = "versand";
    public static final String TABLENAME_BEWERTUNG = "bewertung";
    public static final String TABLENAME_STORNIERUNG = "stornierung";

    public List<Auktion> createVersandAndBewertung(List<Auktion> auktionen) throws IOException {
        List<Versand> versandList = new LinkedList<>();
        int versandId = 1;
        List<String> blindtexts = Files.readAllLines(Paths.get("data/blindtext.txt"));
        int bewertungId = 1;
        int stornierungId = 1;
        for (Auktion auktion : auktionen) {
            versandList = new LinkedList<>();
            if (auktion.getGebote().size() > 0) {
                Random random = new Random();
                int parcels = random.nextInt(3);
                for (int i = 1; i <= parcels; i++) {
                    long lowBound = auktion.getEnde().getTime();
                    if (lowBound < Timestamp.valueOf("2020-12-15 00:00:00").getTime()) {
                        long highBound = Timestamp.from(new Timestamp(lowBound).toInstant().plus(7, ChronoUnit.DAYS)).getTime();
                        long diff = highBound - lowBound + 1;
                        Timestamp versandDatum = new Timestamp(lowBound + (long) (Math.random() * diff));
                        lowBound = Timestamp.from(versandDatum.toInstant().plus(1, ChronoUnit.DAYS)).getTime();
                        highBound = Timestamp.from(new Timestamp(lowBound).toInstant().plus(10, ChronoUnit.DAYS)).getTime();
                        diff = highBound - lowBound + 1;
                        Timestamp ankunftsDatum = new Timestamp(lowBound + (long) (Math.random() * diff));
                        Versand versand;
                        if (ankunftsDatum.getTime() <= Timestamp.valueOf("2020-12-15 00:00:00").getTime()) {
                            versand = new Versand(versandId++, versandDatum, auktion.getId(), ankunftsDatum);
                        } else {
                            versand = new Versand(versandId++, versandDatum, auktion.getId(), null);
                        }
                        versandList.add(versand);
                    }
                }
                int createBewertung = random.nextInt(4);
                if (createBewertung == 1) {
                    Bewertung bewertung = new Bewertung(bewertungId++, random.nextInt(6), blindtexts.get(random.nextInt(blindtexts.size())), auktion.getId(),
                            auktion.getGebote().get(auktion.getGebote().size() - 1).getKaeufer(), auktion.getVerkaeufer());
                    auktion.setBewertung(bewertung);
                }
                int createStornierung = random.nextInt(4);
                if (createStornierung == 1 && hasAnkunftsdaten(versandList)) {
                    Stornierung stornierung = createStornierung(auktion, new Timestamp(versandList.get(versandList.size() - 1).getAnkunftsdatum().getTime()), stornierungId++);
                    long lowBound = stornierung.getTime().getTime();
                    long highBound = Timestamp.from(new Timestamp(lowBound).toInstant().plus(7, ChronoUnit.DAYS)).getTime();
                    long diff = highBound - lowBound + 1;
                    Timestamp versandDatum = new Timestamp(lowBound + (long) (Math.random() * diff));
                    lowBound = Timestamp.from(versandDatum.toInstant().plus(1, ChronoUnit.DAYS)).getTime();
                    highBound = Timestamp.from(new Timestamp(lowBound).toInstant().plus(10, ChronoUnit.DAYS)).getTime();
                    diff = highBound - lowBound + 1;
                    Timestamp ankunftsDatum = new Timestamp(lowBound + (long) (Math.random() * diff));
                    Versand rueckVersand = new Versand(versandId++, versandDatum, auktion.getId(), ankunftsDatum);
                    auktion.setStornierung(stornierung);
                    versandList.add(rueckVersand);
                }

            }
            auktion.setVersand(versandList);
        }
        return auktionen;
    }

    public List<String> createVersandAndBewertungInserts(List<Auktion> auktionen) {
        StringWrapUtil sw = new StringWrapUtil();
        String[] versandColumns = new String[]{"id", "versanddatum", "auktion", "ankunftsdatum"};
        List<String[]> versandAttributes = new LinkedList<>();
        String[] bewertungColumns = new String[]{"id", "sterne", "beschreibung", "auktion", "von", "an"};
        String[] stornierungsColumns = new String[]{"id", "beschreibung", "auktion", "time"};
        List<String[]> stornierungsAttributes = new LinkedList<>();
        List<String[]> bewertungAttributes = new LinkedList<>();
        for (Auktion auktion : auktionen) {
            Bewertung bewertung = auktion.getBewertung();
            Stornierung stornierung = auktion.getStornierung();
            if (auktion.getVersand() != null) {
                for (Versand versand : auktion.getVersand()) {
                    if (versand.getAnkunftsdatum() != null) {
                        versandAttributes.add(new String[]{String.valueOf(versand.getId()), sw.d(versand.getVersanddatum().getTime()),
                                String.valueOf(versand.getAuktion()), sw.d(versand.getAnkunftsdatum().getTime())});
                    } else {
                        versandAttributes.add(new String[]{String.valueOf(versand.getId()), sw.d(versand.getVersanddatum().getTime()),
                                String.valueOf(versand.getAuktion()), "null"});
                    }
                }
            }
            if (auktion.getBewertung() != null) {
                bewertungAttributes.add(new String[]{String.valueOf(bewertung.getId()), String.valueOf(bewertung.getSterne()),
                        sw.w(bewertung.getBeschreibung()), String.valueOf(bewertung.getAuktion()), String.valueOf(bewertung.getVon()), String.valueOf(bewertung.getAn())});
            }
            if (stornierung != null) {
                stornierungsAttributes.add(new String[]{String.valueOf(stornierung.getId()), stornierung.getBeschreibung(),
                        String.valueOf(stornierung.getAuktion()), sw.d(stornierung.getTime().getTime())});
            }

        }
        List<String> versandInserts = InsertStatementCreationUtil.createInsert(versandColumns, versandAttributes, TABLENAME_VERSAND);
        versandInserts.addAll(InsertStatementCreationUtil.createInsert(bewertungColumns, bewertungAttributes, TABLENAME_BEWERTUNG));
        versandInserts.addAll(InsertStatementCreationUtil.createInsert(stornierungsColumns, stornierungsAttributes, TABLENAME_STORNIERUNG));
        return versandInserts;
    }

    public Stornierung createStornierung(Auktion auktion, Timestamp ankunftsDatum, int stornierungId) throws IOException {
        DateCreationUtil dcU = new DateCreationUtil();
        StringWrapUtil sw = new StringWrapUtil();
        List<String> blindtexts = Files.readAllLines(Paths.get("data/stornierungsgrund.txt"));
        Random random = new Random();
        return new Stornierung(stornierungId, sw.w(blindtexts.get(random.nextInt(blindtexts.size()))), auktion.getId(), dcU.getRandomDateInRange(4, ankunftsDatum));
    }

    public boolean hasAnkunftsdaten(List<Versand> versandList) {
        if (versandList.size() == 0) {
            return false;
        }
        for (Versand versand : versandList) {
            if (versand.getAnkunftsdatum() == null) {
                return false;
            }
        }
        return true;
    }
}
