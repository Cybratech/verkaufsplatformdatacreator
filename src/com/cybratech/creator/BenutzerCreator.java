package com.cybratech.creator;

import com.cybratech.InsertStatementCreationUtil;
import com.cybratech.StringWrapUtil;
import com.cybratech.dto.Adresse;
import com.cybratech.dto.Benutzer;
import sun.awt.image.ImageWatched;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class BenutzerCreator {
    private static final String TABLENAME = "benutzer";

    public List<Benutzer> createBenutzer(int number) throws IOException {
        List<Benutzer> benutzers = new LinkedList<>();
        List<String> namen = Files.readAllLines(Paths.get("data/names.txt"));
        String[] name;
        Random random = new Random();
        for (int i = 1; i <= number; i++) {
            name = namen.get(random.nextInt(namen.size())).split(",");
            Benutzer benutzer = new Benutzer(i, String.join(" ", name), i);
            benutzers.add(benutzer);
        }
        return benutzers;
    }

    public List<String> createBenutzerInserts(List<Benutzer> benutzers){
        StringWrapUtil sw = new StringWrapUtil();
        String[] columns = new String[]{"id","name","adresse"};
        List<String[]> attributes = new LinkedList<>();
        for (Benutzer nutzer : benutzers) {
            attributes.add(new String[]{String.valueOf(nutzer.getId()), sw.w(nutzer.getName()),String.valueOf(nutzer.getAdresse())});
        }
        return InsertStatementCreationUtil.createInsert(columns, attributes, TABLENAME);
    }
}
