package com.cybratech.creator;

import com.cybratech.InsertStatementCreationUtil;
import com.cybratech.StringWrapUtil;
import com.cybratech.dto.Kategorie;

import java.util.LinkedList;
import java.util.List;

public class KategorieCreator {
    String[] categories = new String[]{"Fahrrad", "Auto", "Buch"};
    String[] attributes = new String[]{"gelbes", "rotes", "grünes", "weißes", "blaues", "schwarzes", "rotes", "silbernes"};
    String[] attributesP = new String[]{"gelbe", "rote", "grüne", "weiße", "blaue", "schwarze", "rote", "silberne"};
    String[] categoriesP = new String[]{"Lautsprecher", "Tastatur", "Lautsprecher"};
    private static final String TABLENAME = "kategorie";

    /**
     * Creates Category Objects.
     *
     * @param number the number of categories that should be created
     * @return List of categories.
     */
    public List<Kategorie> createCategories(Integer number) {
        List<Kategorie> kategorieObjects = new LinkedList<>();
        long id = 1;
        for (String catS : categories) {
            for (String argS : attributes) {
                Kategorie kategorie = new Kategorie(id, argS + " " + catS);
                kategorieObjects.add(kategorie);
                if (id < number) {
                    id++;
                } else {
                    break;
                }
            }
            if (id >= number) {
                break;
            }
        }
        if (id < number) {
            for (String catP : categoriesP) {
                for (String argP : attributesP) {
                    Kategorie kategorie = new Kategorie(id, argP + " " + catP);
                    kategorieObjects.add(kategorie);
                    if (id < number) {
                        id++;
                    } else {
                        break;
                    }
                }
                if (id >= number) {
                    break;
                }
            }
        }
        return kategorieObjects;
    }

    /**
     * Creates the Insert Statements for the categories.
     *
     * @param categories the category List
     * @return List with insert statements
     */
    public List<String> createCategoryInserts(List<Kategorie> categories) {
        String[] columns = new String[]{"id", "name"};
        StringWrapUtil sw = new StringWrapUtil();
        List<String[]> attributes = new LinkedList<>();
        for (Kategorie cat : categories) {
            attributes.add(new String[]{String.valueOf(cat.getId()), sw.w(cat.getName())});
        }
        return InsertStatementCreationUtil.createInsert(columns, attributes, TABLENAME);
    }
}
