package com.cybratech.creator;

import com.cybratech.InsertStatementCreationUtil;
import com.cybratech.StringWrapUtil;
import com.cybratech.dto.Adresse;
import com.cybratech.dto.Kategorie;
import com.cybratech.dto.Produkt;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ProduktCreator {
    private static final String TABLENAME = "produkt";

    public List<Produkt> createProdukte(List<Kategorie> kategorieList, int number) throws IOException {
        List<Produkt> produktList = new LinkedList<>();
        List<String> blindtexts = Files.readAllLines(Paths.get("data/blindtext.txt"));
        Random rand = new Random();
        for (int i = 1; i <= number; i++) {
            Produkt produkt = new Produkt(i, blindtexts.get(rand.nextInt(blindtexts.size())),
                    kategorieList.get(rand.nextInt(kategorieList.size())).getId());
            produktList.add(produkt);
        }
        return produktList;
    }

    public List<String> createProdukteInsert(List<Produkt> produkte){
        StringWrapUtil sw = new StringWrapUtil();
        String[] columns = new String[]{"id", "beschreibung", "kategorie"};
        List<String[]> attributes = new LinkedList<>();
        for (Produkt product : produkte) {
            attributes.add(new String[]{String.valueOf(product.getId()), sw.w(product.getBeschreibung()),
                    String.valueOf(product.getKategorie())});
        }
        return InsertStatementCreationUtil.createInsert(columns, attributes, TABLENAME);
    }
}
