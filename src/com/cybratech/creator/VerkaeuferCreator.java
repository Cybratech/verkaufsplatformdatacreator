package com.cybratech.creator;

import com.cybratech.InsertStatementCreationUtil;
import com.cybratech.dto.Benutzer;
import com.cybratech.dto.Kaeufer;
import com.cybratech.dto.Verkaeufer;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class VerkaeuferCreator {

    private static final String TABLENAME="verkaeufer";

    public List<Verkaeufer> createVerkaeufer(List<Benutzer> benutzers){
        List<Verkaeufer> verkaeufers = new LinkedList<>();
        Random random = new Random();
        for(Benutzer nutzer: benutzers){
            Verkaeufer verkaeufer = new Verkaeufer(nutzer.getId(),random.nextInt(10));
            verkaeufers.add(verkaeufer);
        }
        return verkaeufers;
    }

    public List<String> createVerkaeuferInserts(List<Verkaeufer> verkaeufers){
        String[] columns = new String[]{"id"};
        List<String[]> attributes = new LinkedList<>();
        for (Verkaeufer verk : verkaeufers) {
            attributes.add(new String[]{String.valueOf(verk.getId())});
        }
        return InsertStatementCreationUtil.createInsert(columns, attributes, TABLENAME);
    }
}
