package com.cybratech.creator;

import com.cybratech.InsertStatementCreationUtil;
import com.cybratech.StringWrapUtil;
import com.cybratech.dto.Adresse;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class AdresseCreator {
    private static final String TABLENAME = "adresse";

    public List<Adresse> createAdressen(int number) throws IOException {
        List<Adresse> adresses = new LinkedList<>();
        List<String> addressLines = Files.readAllLines(Paths.get("data/zipcodes.csv"));
        List<String> streetLines = Files.readAllLines(Paths.get("data/straßennamen.txt"));
        Random random = new Random();
        String[] addressLine;
        String city;
        String zipcode;
        String streetname;
        for (int i = 1; i <= number; i++) {
            addressLine = addressLines.get(random.nextInt(addressLines.size())).split(",");
            streetname = streetLines.get(random.nextInt(streetLines.size()));
            zipcode = addressLine[1];
            city = addressLine[2];
            Adresse adresse = new Adresse(i, streetname, String.valueOf(random.nextInt(100 - 1 + 1) + 1), Integer.valueOf(zipcode), city);
            adresses.add(adresse);
        }
        return adresses;
    }

    public List<String> createAdressenInserts(List<Adresse> adresses) {
        StringWrapUtil sw = new StringWrapUtil();
        String[] columns = new String[]{"id", "strasse", "hausnummer", "plz", "ort"};
        List<String[]> attributes = new LinkedList<>();
        for (Adresse adress : adresses) {
            attributes.add(new String[]{String.valueOf(adress.getId()), sw.w(adress.getStrasse()),
                    sw.w(adress.getHausnummer()), String.valueOf(adress.getPlz()), sw.w(adress.getOrt())});
        }
        return InsertStatementCreationUtil.createInsert(columns, attributes, TABLENAME);
    }
}
