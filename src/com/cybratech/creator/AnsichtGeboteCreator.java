package com.cybratech.creator;

import com.cybratech.DateCreationUtil;
import com.cybratech.InsertStatementCreationUtil;
import com.cybratech.StringWrapUtil;
import com.cybratech.dto.*;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class AnsichtGeboteCreator {

    private static final String TABLENAME_ANSICHT = "ansichten";
    private static final String TABLENAME_GEBOT = "gebot";

    public List<Auktion> createAnsichtenAndGebote(List<Auktion> auktionen, List<Kaeufer> kaeufers) {
        DateCreationUtil dCU = new DateCreationUtil();
        List<Ansicht> ansichten = new LinkedList<Ansicht>();
        List<Gebot> gebote = new LinkedList<Gebot>();
        Random random = new Random();
        int ansichtId = 1;
        int gebotId = 1;
        for (Auktion auktion : auktionen) {
            //Ansichten pro Auktion
            long lowBound = auktion.getStart().getTime();
            long highBound = auktion.getEnde().getTime();
            long diff = highBound - lowBound + 1;
            double letzterPreis = auktion.getStartpreis();
            Timestamp ansichtDate;
            long computedDiff;
            for (int i = 0; i < random.nextInt(50); i++) {
                ansichtDate = new Timestamp(lowBound + (long) (Math.random() * diff));
                lowBound = ansichtDate.getTime();
                diff = highBound - lowBound + 1;
                long kaeufer = kaeufers.get(random.nextInt(kaeufers.size())).getId();
                Ansicht ansicht = new Ansicht(ansichtId++, ansichtDate, kaeufer, auktion.getId());
                // 1/3, dass eine Ansicht ein Gebot nach sich zieht
                if (random.nextInt(3) == 1) {
                    double gebotsPreis = letzterPreis + random.nextInt(25);
                    Gebot gebot = new Gebot(gebotId++, letzterPreis, ansichtDate, auktion.getId(), kaeufer);
                    letzterPreis = gebotsPreis;
                    gebote.add(gebot);
                }
                ansichten.add(ansicht);
            }
            auktion.setAnsichten(ansichten);
            auktion.setGebote(gebote);
            gebote = new LinkedList<>();
            ansichten = new LinkedList<>();
        }
        return auktionen;
    }

    public List<String> createAnsichtsAndGeboteInserts(List<Auktion> auktionen) {
        StringWrapUtil sw = new StringWrapUtil();
        String[] anischtsColumns = new String[]{"id", "zeitpunkt", "kaufer", "auktion"};
        List<String[]> ansichtsAttributes = new LinkedList<>();
        String[] gebotsColumns = new String[]{"id", "preis", "zeitpunkt", "auktion", "kaeufer"};
        List<String[]> gebotsAttributes = new LinkedList<>();
        for (Auktion auktion : auktionen) {
            for (Ansicht ansicht : auktion.getAnsichten()) {
                ansichtsAttributes.add(new String[]{String.valueOf(ansicht.getId()), sw.d(ansicht.getZeitpunkt().getTime()),
                        String.valueOf(ansicht.getKaufer()), String.valueOf(ansicht.getAuktion())});
            }
            for (Gebot gebot : auktion.getGebote()) {
                gebotsAttributes.add(new String[]{String.valueOf(gebot.getId()), String.valueOf(gebot.getPreis()),
                        sw.d(gebot.getZeitpunkt().getTime()), String.valueOf(gebot.getAuktion()), String.valueOf(gebot.getKaeufer())});
            }
        }

        List<String> ansichtsInserts = InsertStatementCreationUtil.createInsert(anischtsColumns, ansichtsAttributes, TABLENAME_ANSICHT);
        ansichtsInserts.addAll(InsertStatementCreationUtil.createInsert(gebotsColumns, gebotsAttributes, TABLENAME_GEBOT));
        return ansichtsInserts;
    }

}
