package com.cybratech.creator;

import com.cybratech.InsertStatementCreationUtil;
import com.cybratech.StringWrapUtil;
import com.cybratech.dto.Auktion;
import com.cybratech.dto.Kategorie;
import com.cybratech.dto.Produkt;
import com.cybratech.dto.Verkaeufer;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class AuktionCreator {
    private static final String TABLENAME = "auktion";

    public List<Auktion> createAuktionen(int number, List<Produkt> produkte, List<Verkaeufer> verkaeufers) {
        List<Auktion> auktionen = new LinkedList<>();
        long lowBound = Timestamp.valueOf("2019-06-01 00:00:00").getTime();
        long highBound = Timestamp.valueOf("2020-12-15 00:00:00").getTime();
        long diff = highBound - lowBound + 1;
        long computedDiff;
        Random random = new Random();
        Timestamp auktionBeginDate;
        Timestamp currentUpperBound;
        Timestamp auktionEndDate;
        for (int id = 1; id <= number; id++) {
            auktionBeginDate = new Timestamp(lowBound + (long) (Math.random() * diff));
            currentUpperBound = Timestamp.from(auktionBeginDate.toInstant().plus(7, ChronoUnit.DAYS));
            computedDiff = currentUpperBound.getTime() - auktionBeginDate.getTime() + 1;
            auktionEndDate = new Timestamp(auktionBeginDate.getTime() + (long) (Math.random() * computedDiff));
            Auktion auktion = new Auktion(id, produkte.get(random.nextInt(produkte.size())).getId(), new Date(auktionBeginDate.getTime()),
                    new Date(auktionEndDate.getTime()), random.nextInt(10000), verkaeufers.get(random.nextInt(verkaeufers.size())).getId());
            auktionen.add(auktion);
        }
        return auktionen;
    }

    public List<String> createAuktionenInserts(List<Auktion> auktionen) {
        StringWrapUtil sw = new StringWrapUtil();
        String[] columns = new String[]{"id", "produkt", "start", "ende", "startpreis", "verkaeufer"};
        List<String[]> attributes = new LinkedList<>();
        for (Auktion auktion : auktionen) {
            attributes.add(new String[]{String.valueOf(auktion.getId()), String.valueOf(auktion.getProdukt()),
                    sw.d(auktion.getStart().getTime()), sw.d(auktion.getEnde().getTime()), String.valueOf(auktion.getStartpreis()),
                    String.valueOf(auktion.getVerkaeufer())});
        }
        return InsertStatementCreationUtil.createInsert(columns, attributes, TABLENAME);
    }
}
