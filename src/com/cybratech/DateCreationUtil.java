package com.cybratech;

import com.cybratech.dto.Versand;

import java.sql.Timestamp;
import java.time.temporal.ChronoUnit;

public class DateCreationUtil {

    /**
     * Generate a new Timestamp in between the given and the days to add.
     *
     * @param days              the max days in which the time is returned
     * @param currentTimpestamp currentTime for the generation
     * @return the timestamp
     */
    public Timestamp getRandomDateInRange(int days, Timestamp currentTimpestamp) {
        long lowBound = currentTimpestamp.getTime();
        long highBound = Timestamp.from(new Timestamp(lowBound).toInstant().plus(days, ChronoUnit.DAYS)).getTime();
        long diff = highBound - lowBound + 1;
        return new Timestamp(lowBound + (long) (Math.random() * diff));
    }

    /**
     * Generate a new Timestamp in between the given ones.
     *
     * @param high max time for generation
     * @param low  low time for the generation
     * @return the timestamp
     */
    public Timestamp getRandomDateInRange(Timestamp low, Timestamp high) {
        long lowBound = low.getTime();
        long highBound = high.getTime();
        long diff = highBound - lowBound + 1;
        return new Timestamp(lowBound + (long) (Math.random() * diff));
    }
}
