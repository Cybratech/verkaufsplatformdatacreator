package com.cybratech.dto;

import java.util.Date;

public class Gebot {
    private long id;
    private double preis;
    private Date zeitpunkt;
    private long auktion;
    private long kaeufer;

    public Gebot(long id, double preis, Date zeitpunkt, long auktion, long kaeufer) {
        this.id = id;
        this.preis = preis;
        this.zeitpunkt = zeitpunkt;
        this.auktion = auktion;
        this.kaeufer = kaeufer;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPreis() {
        return preis;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    public Date getZeitpunkt() {
        return zeitpunkt;
    }

    public void setZeitpunkt(Date zeitpunkt) {
        this.zeitpunkt = zeitpunkt;
    }

    public long getAuktion() {
        return auktion;
    }

    public void setAuktion(long auktion) {
        this.auktion = auktion;
    }

    public long getKaeufer() {
        return kaeufer;
    }

    public void setKaeufer(long kaeufer) {
        this.kaeufer = kaeufer;
    }
}
