package com.cybratech.dto;

import java.util.Date;

public class Ansicht {
    private long id;
    private Date zeitpunkt;
    private long kaufer;
    private long auktion;

    public Ansicht(long id, Date zeitpunkt, long kaufer, long auktion) {
        this.id = id;
        this.zeitpunkt = zeitpunkt;
        this.kaufer = kaufer;
        this.auktion = auktion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getZeitpunkt() {
        return zeitpunkt;
    }

    public void setZeitpunkt(Date zeitpunkt) {
        this.zeitpunkt = zeitpunkt;
    }

    public long getKaufer() {
        return kaufer;
    }

    public void setKaufer(long kaufer) {
        this.kaufer = kaufer;
    }

    public long getAuktion() {
        return auktion;
    }

    public void setAuktion(long auktion) {
        this.auktion = auktion;
    }
}
