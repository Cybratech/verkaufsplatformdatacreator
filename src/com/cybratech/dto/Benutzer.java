package com.cybratech.dto;

public class Benutzer {
    private long id;
    private String name;
    private long adresse;

    public Benutzer(long id, String name, long adresse) {
        this.id = id;
        this.name = name;
        this.adresse = adresse;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getAdresse() {
        return adresse;
    }

    public void setAdresse(long adresse) {
        this.adresse = adresse;
    }
}
