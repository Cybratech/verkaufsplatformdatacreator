package com.cybratech.dto;

public class Produkt {
    private long id;
    private String beschreibung;
    private long kategorie;

    public Produkt(long id, String beschreibung, long kategorie) {
        this.id = id;
        this.beschreibung = beschreibung;
        this.kategorie = kategorie;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public long getKategorie() {
        return kategorie;
    }

    public void setKategorie(long kategorie) {
        this.kategorie = kategorie;
    }
}
