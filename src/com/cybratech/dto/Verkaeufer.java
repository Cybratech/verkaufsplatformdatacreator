package com.cybratech.dto;

public class Verkaeufer {
    private long id;
    private double provision;

    public Verkaeufer(long id, double provision) {
        this.id = id;
        this.provision = provision;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getProvision() {
        return provision;
    }

    public void setProvision(double provision) {
        this.provision = provision;
    }
}
