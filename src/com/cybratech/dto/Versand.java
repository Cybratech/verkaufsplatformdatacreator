package com.cybratech.dto;

import java.util.Date;

public class Versand {
    private long id;
    private Date versanddatum;
    private long auktion;
    private Date ankunftsdatum;

    public Versand(long id, Date versanddatum, long auktion, Date ankunftsdatum) {
        this.id = id;
        this.versanddatum = versanddatum;
        this.auktion = auktion;
        this.ankunftsdatum = ankunftsdatum;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getVersanddatum() {
        return versanddatum;
    }

    public void setVersanddatum(Date versanddatum) {
        this.versanddatum = versanddatum;
    }

    public long getAuktion() {
        return auktion;
    }

    public void setAuktion(long auktion) {
        this.auktion = auktion;
    }

    public Date getAnkunftsdatum() {
        return ankunftsdatum;
    }

    public void setAnkunftsdatum(Date ankunftsdatum) {
        this.ankunftsdatum = ankunftsdatum;
    }
}
