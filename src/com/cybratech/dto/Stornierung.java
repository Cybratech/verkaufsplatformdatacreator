package com.cybratech.dto;

import java.sql.Timestamp;

public class Stornierung {
    long id;
    String beschreibung;
    long auktion;
    Timestamp time;

    public Stornierung(long id, String beschreibung, long auktion, Timestamp time) {
        this.id = id;
        this.beschreibung = beschreibung;
        this.auktion = auktion;
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public long getAuktion() {
        return auktion;
    }

    public void setAuktion(long auktion) {
        this.auktion = auktion;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
