package com.cybratech.dto;

public class Bewertung {
    private long id;
    private int sterne;
    private String beschreibung;
    private long auktion;
    private long von;
    private long an;

    public Bewertung(long id, int sterne, String beschreibung, long auktion, long von, long an) {
        this.id = id;
        this.sterne = sterne;
        this.beschreibung = beschreibung;
        this.auktion = auktion;
        this.von = von;
        this.an = an;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getSterne() {
        return sterne;
    }

    public void setSterne(int sterne) {
        this.sterne = sterne;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public long getAuktion() {
        return auktion;
    }

    public void setAuktion(long auktion) {
        this.auktion = auktion;
    }

    public long getVon() {
        return von;
    }

    public void setVon(long von) {
        this.von = von;
    }

    public long getAn() {
        return an;
    }

    public void setAn(long an) {
        this.an = an;
    }
}
