package com.cybratech.dto;

import java.util.Date;
import java.util.List;


public class Auktion {
    private long id;
    private long produkt;
    private Date start;
    private Date ende;
    private double startpreis;
    private long verkaeufer;
    List<Gebot> gebote;
    List<Ansicht> ansichten;
    List<Versand> versand;
    Bewertung bewertung;
    private Stornierung stornierung;

    public Auktion(long id, long produkt, Date start, Date ende, double startpreis, long verkaeufer) {
        this.id = id;
        this.produkt = produkt;
        this.start = start;
        this.ende = ende;
        this.startpreis = startpreis;
        this.verkaeufer = verkaeufer;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProdukt() {
        return produkt;
    }

    public void setProdukt(long produkt) {
        this.produkt = produkt;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnde() {
        return ende;
    }

    public void setEnde(Date ende) {
        this.ende = ende;
    }

    public double getStartpreis() {
        return startpreis;
    }

    public void setStartpreis(double startpreis) {
        this.startpreis = startpreis;
    }

    public long getVerkaeufer() {
        return verkaeufer;
    }

    public void setVerkaeufer(long verkaeufer) {
        this.verkaeufer = verkaeufer;
    }

    public List<Gebot> getGebote() {
        return gebote;
    }

    public void setGebote(List<Gebot> gebote) {
        this.gebote = gebote;
    }

    public List<Ansicht> getAnsichten() {
        return ansichten;
    }

    public void setAnsichten(List<Ansicht> ansichten) {
        this.ansichten = ansichten;
    }

    public List<Versand> getVersand() {
        return versand;
    }

    public void setVersand(List<Versand> versand) {
        this.versand = versand;
    }

    public Bewertung getBewertung() {
        return bewertung;
    }

    public void setBewertung(Bewertung bewertung) {
        this.bewertung = bewertung;
    }

    public Stornierung getStornierung() {
        return stornierung;
    }

    public void setStornierung(Stornierung stornierung) {
        this.stornierung = stornierung;
    }
}
