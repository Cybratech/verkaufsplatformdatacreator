package com.cybratech.dto;

public class Kaeufer {
    private long id;

    public Kaeufer(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
