package com.cybratech;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class InsertStatementCreationUtil {

    private static final String basicInsert = "INSERT INTO `%s` (%s) VALUES (%s);";

    /**
     * Creates the insert Statements with the given arguments.
     *
     * @param columns name of the columns.
     * @param arguments List of arguments
     * @param tableName the table name
     * @return the List with insert Statements
     */
    public static List<String> createInsert(String[] columns, List<String[]> arguments, String tableName) {
        List<String> returnInserts = new LinkedList<>();
        String columnString = String.join(", ", columns);
        for (String[] args : arguments) {
            returnInserts.add(String.format(basicInsert,tableName, columnString, String.join(", ", args)));
        }
        return returnInserts;
    }

}
